#include <stdio.h>

#define MAX_STR	20

void swapper_v1(int *a, int *b);

int main(int argc, char *argv[])
{
	int a, b;
	char name[MAX_STR];

	printf("Pleas enter two numbers: ");
	scanf("%d %d", &a, &b);

	swapper_v1(a,b);

	printf("Swapping is completed. What's your name? ");
	scanf("%s", name);

	printf("Ok, %s. Good job!\n", name);

	return 0;
}

void swapper_v1(int *a, int *b)
{
	int local_a, local_b;

	local_a = *a;
	local_b = *b;

	*a = local_b;
	*b = local_a;
}
